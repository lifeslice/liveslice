import numpy as np
from os.path import exists
from live_slice.edf_io.EdfFile import *
from pylab import *
import re
import os
import sys

def read_proj_PCO_multiedf(direc, prefix, slices, noref=False, verbose=False):
    """
    Read the projections, the flat and the dark from multiedf files.

    Parameters
    ----------

    direc : string
        Directory of the proj
 
    prefix : string
        Prefix of the name of the proj

    slices : ndaaray
        Index of the slices to read

    noref : boolean, default=False
        noref=False : look to find dark and flat    
        noref=True : skip dark and flat

    verbose : boolean, default=False
        
    Outputs
    -------
    dd : ndarray of float
        Median value of the dark

    ff : ndarray of float
        Median value of the flat
    
    pp : ndarray of float
        Sinogram
 
    Example
    -------
    >>> ????

    """

    if(not noref):
        #Define default values for flat and dark name
        fn_flat = "refHST0000.edf"
        fn_dark = 'darkend0000.edf'


        #Check if a median has already been performed for trash and dark
        if exists("%s/dark.edf" %(direc)):
            fn_dark = 'dark.edf'
            ndark = 1

        if exists("%s/flat.edf"%(direc)):
            fn_flat = 'flat.edf'
            nflat = 1

        #Dark field
        sys.stdout.write("\nRead dark field ....")
        file_dark = "%s/%s" %(direc,fn_dark)
        if not exists("%s" %(file_dark)):
            reg1=re.compile(".*DARK.*.edf")
            reg2=re.compile(".*Dark.*.edf")
            reg3=re.compile(".*dark.*.edf")
            filelist=os.listdir(direc)
            file_dark_list=[m.group(0) for l in filelist for m in [reg1.search(l),reg2.search(l),reg3.search(l)] if m]        
            file_dark='%s/%s' %(direc,file_dark_list[0])
        if(verbose):
            print file_dark
        dark = EdfFile(file_dark)
        hd = dark.GetStaticHeader(0) 

        dim1 = int(hd['Dim_1'])
        dim2 = int(hd['Dim_2'])
        datatype=hd['DataType']
        ndark = dark.GetNumImages()
        d = np.zeros((len(slices),dim2,ndark))
        #d = np.zeros((dim2,len(slices),ndark))
        #d = np.zeros((dim2,dim1,ndark))
        for i in range(0,ndark):
            #d[:,:,i] = dark.GetData(i,datatype,(slices[0],0),(len(slices),dim2))
            d[:,:,i] = dark.GetData(i,datatype,(0,slices[0]),(dim2,len(slices)))
            sys.stdout.write("\rRead dark field ... %d/%d" %(i+1,ndark) )

        del dark

        if(ndark>1):
            print "\nMedian dark fields ....\n"
            d = np.median(d,axis=2)
            #newdark = EdfFile("%s/dark.edf" %(direc))
            #newdark.WriteImage (hd, d, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
            #del newdark

        #Flat field
        sys.stdout.write("\nRead flat field ....")
        file_flat = "%s/%s" %(direc,fn_flat);
        if not exists("%s" %(file_flat)):
            reg1=re.compile(".*FLAT.*.edf")
            reg2=re.compile(".*Flat.*.edf")
            reg3=re.compile(".*flat.*.edf")
            reg4=re.compile(".*refHST.*.edf")
            reg5=re.compile(".*ref.*.edf")
            filelist=os.listdir(direc)
            file_flat_list=[m.group(0) for l in filelist for m in [reg1.search(l),reg2.search(l),reg3.search(l),reg4.search(l),reg5.search(l)] if m]        
            file_flat='%s/%s' %(direc,file_flat_list[0])
        if(verbose):
            print file_flat
        flat = EdfFile(file_flat)
        hd = flat.GetStaticHeader(0) 

        dim1 = int(hd['Dim_1'])
        dim2 = int(hd['Dim_2'])
        datatype=hd['DataType']
        nflat = flat.GetNumImages()
        #f = np.zeros((dim2,len(slices),nflat))
        f = np.zeros((len(slices),dim2,nflat))
        #f = np.zeros((dim2,dim1,nflat))
        for i in range(0,nflat):
            f[:,:,i] = flat.GetData(i,datatype,(0,slices[0]),(dim2,len(slices)))
            #f[:,:,i] = flat.GetData(i)
            sys.stdout.write("\rRead flat fields .... %d/%d" %(i+1,nflat))

        del flat

        if(nflat>1):
            print "\nMedian flat fields ....\n"
            f = np.median(f,axis=2) 
            #newflat = EdfFile("%s/flat.edf" %(direc))
            #newflat.WriteImage (hd, f, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
            #del newflat

        f=f.squeeze()
        d=d.squeeze()

        f=abs(f-d)  

        if(verbose):
            print "\n Dark and flat size is ", f.shape
    
    
    #Projections
    sys.stdout.write("\nRead projections ....")
    file_im = "%s/%s.edf" %(direc,prefix);
    if(verbose):
        print file_im
    projec = EdfFile(file_im)
    hd = projec.GetStaticHeader(0) 

    dim1 = int(hd['Dim_1'])
    dim2 = int(hd['Dim_2'])
    datatype=hd['DataType']
    print "datatype :" , datatype
    nproj = projec.GetNumImages()
  
    if(True):
        debi=projec.GetData(1000,datatype,(500,0),(1000,2016))
        
        fn = "%s/%s_test.edf" %(direc,prefix)
        testsino = EdfFile("%s" %(fn))
        testsino.WriteImage ({}, debi, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
        del testsino
    
    #ii = np.zeros((dim2,len(slices),nproj))
    ii = np.zeros((len(slices),dim2,nproj))
    #ii = np.zeros((dim2,dim1,nproj))
    for i in range(0,nproj):
        #ii[:,:,i] = projec.GetData(i,datatype,(slices[0],0),(len(slices),dim2))
        ii[:,:,i] = projec.GetData(i,datatype,(0,slices[0]),(dim2,len(slices)))
        #ii[:,:,i] = projec.GetData(i)
        sys.stdout.write("\rRead proj .... %d/%d" %(i+1,nproj))

    #Closure of edf projection file
    del projec

    if(noref):
        d = np.zeros((len(slices),dim2))
        f = 256*256*np.ones((len(slices),dim2))

    #dd = d[slices,:]
    #ff = f[slices,:]
    #pp = ii[slices,:]

    print "\n\n---> All data successfully imported.\n"
    return (d, f, ii) 

