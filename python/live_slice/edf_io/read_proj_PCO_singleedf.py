import numpy as np
from os.path import exists
from live_slice.edf_io.EdfFile import *
from pylab import *
import re
import os
import sys
import subprocess

def read_proj_PCO_singleedf(file_name, verbose=False):
    """
    Read a a singleedf file.

    Parameters
    ----------


    verbose : boolean, default=False
        
    Outputs
    -------
    
    i : ndarray of int
        Sinogram
 
    Example
    -------
    >>> ????

    """

  
    #Projections
    sys.stdout.write("\nRead sinogram ....")
    projec = EdfFile(file_name)
    hd = projec.GetStaticHeader(0) 

    dim1 = int(hd['Dim_1'])
    dim2 = int(hd['Dim_2'])
    i = np.zeros((dim2,dim1))
    i = projec.GetData(0)
    sys.stdout.write("\rRead sinogram .... %d/%d" %(1,1))

    #Closure of edf projection file
    del projec

    print "\n\n---> All data successfully imported.\n"

    return i 

