import numpy as np
from os.path import exists
from live_slice.edf_io.EdfFile import *
from pylab import *
import re
import os
import sys

def read_sino_PCO_singleedf(direc, prefix, scan_index, noref=False, verbose=False):
    """
    Read a sinogram, the flat and the dark from a singleedf file.

    Parameters
    ----------

    direc : string
        Directory of the sinogram
 
    prefix : string
        Prefix of the name of the sinogram

    scan_index : int
        Index of the scan

    noref : boolean, default=False
        noref=False : look to find dark and flat    
        noref=True : skip dark and flat

    verbose : boolean, default=False
        
    Outputs
    -------
    d : ndarray of float
        Median value of the dark

    f : ndarray of float
        Median value of the flat
    
    i : ndarray of float
        Sinogram
 
    Example
    -------
    >>> ????

    """

    if(not noref):
        #Define default values for flat and dark name
        fn_flat = "%s%.04d_ref_sino.edf" %(prefix,scan_index)
        fn_dark = "%s%.04d_dark_sino.edf" %(prefix,scan_index)


        #Check if a median has already been performed for trash and dark
        if exists("%s/dark_sino.edf" %(direc)):
            fn_dark = 'dark_sino.edf'
            ndark = 1

        if exists("%s/flat_sino.edf"%(direc)):
            fn_flat = 'flat_sino.edf'
            nflat = 1

        #Dark field
        print "\nRead dark fields ...."
        file_dark = "%s/%s" %(direc,fn_dark)
        if not exists("%s" %(file_dark)):
            reg1=re.compile(".*DARK.*sino.*.edf")
            reg2=re.compile(".*Dark.*sino.*.edf")
            reg3=re.compile(".*dark.*sino.*.edf")
            filelist=os.listdir(direc)
            file_dark_list=[m.group(0) for l in filelist for m in [reg1.search(l),reg2.search(l),reg3.search(l)] if m]        
            file_dark='%s/%s' %(direc,file_dark_list[0])
        if(verbose):
            print file_dark
        dark = EdfFile(file_dark)
        hd = dark.GetStaticHeader(0) 

        dim1 = int(hd['Dim_1'])
        if('Dim_2' in hd):
            dim2 = int(hd['Dim_2'])
        else:
            dim2=1
            
        ndark = dark.GetNumImages()
        print("dim1= %d, dim2=%d, ndark=%d\n" %(dim1,dim2,ndark))

        d = np.zeros((dim2,dim1,ndark))
        for i in range(0,ndark):
            d[:,:,i] = dark.GetData(i)
            print "\rRead dark fields .... %d/%d" %(i+1,ndark) 

        del dark

        if((ndark>1)|(dim2>1)):
            print "\nMedian dark fields ....\n"
            print(d.shape)
            d = np.median(d,axis=2)
            print(d.shape)
            d = np.median(d.T,axis=1)
            print(d.shape)
            newdark = EdfFile("%s/dark_sino.edf" %(direc))
            #hd['Dim_2']='1'
            newdark.WriteImage ({}, d, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
            del newdark

        #Flat field
        print "\nRead flat fields ...."
        file_flat = "%s/%s" %(direc,fn_flat);
        if not exists("%s" %(file_flat)):
            reg1=re.compile(".*FLAT.*sino.*.edf")
            reg2=re.compile(".*Flat.*sino.*.edf")
            reg3=re.compile(".*flat.*sino.*.edf")
            reg4=re.compile(".*refHST.*sino.*.edf")
            reg5=re.compile(".*ref.*sino.*.edf")
            filelist=os.listdir(direc)
            file_flat_list=[m.group(0) for l in filelist for m in [reg1.search(l),reg2.search(l),reg3.search(l),reg4.search(l),reg5.search(l)] if m]        
            file_flat='%s/%s' %(direc,file_flat_list[0])
        if(verbose):
            print file_flat
        flat = EdfFile(file_flat)
        hd = flat.GetStaticHeader(0) 

        dim1 = int(hd['Dim_1'])
        if('Dim_2' in hd):
            dim2 = int(hd['Dim_2'])
        else:
            dim2=1
        nflat = flat.GetNumImages()
        f = np.zeros((dim2,dim1,nflat))
        for i in range(0,nflat):
            f[:,:,i] = flat.GetData(i)
            sys.stdout.write("\rRead flat fields .... %d/%d" %(i+1,nflat))

        del flat

        if((nflat>1)|(dim2>1)):
            print "\nMedian flat fields ....\n"
            f = np.median(f,axis=2) 
            f = np.median(f.T,axis=1)
            newflat = EdfFile("%s/flat_sino.edf" %(direc))
            newflat.WriteImage ({}, f, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
            del newflat

        f=f.squeeze()
        d=d.squeeze()

        f=abs(f-d)  

        if(verbose):
            print "\n Dark and flat size is ", f.shape
    
    
    #Projections
    sys.stdout.write("\nRead sinogram ....")
    file_im = "%s/%s%.04d.edf" %(direc,prefix,scan_index);
    projec = EdfFile(file_im)
    hd = projec.GetStaticHeader(0) 

    dim1 = int(hd['Dim_1'])
    dim2 = int(hd['Dim_2'])
    i = np.zeros((dim2,dim1))
    i = projec.GetData(0)
    sys.stdout.write("\rRead sinogram .... %d/%d" %(1,1))

    #Closure of edf projection file
    del projec

    if(noref):
    	sys.stdout.write("\nNot using any reference\n")
        ma = np.max(np.max(i)) 
        mi = np.min(np.min(i)) 
        #d = np.ones((dim2,))*mi
        #f = np.ones((dim2,))*ma
        d = np.zeros((dim1,))
	f = 256*256*np.ones((dim1,))
	#d=d.squeeze()
	#d=f.squeeze()
        #newdark = EdfFile("%s/dark_sino.edf" %(direc))
        #newdark.WriteImage ({}, d, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
        #del newdark
        #newflat = EdfFile("%s/flat_sino.edf" %(direc))
        #newflat.WriteImage ({}, f, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
        #del newflat

    print "\n\n---> All data successfully imported.\n"

    return (d, f, i) 

def read_more_sino_PCO_singleedf(direc, prefix, scan_index, verbose=False):
    """
    Read a sinogram from a singleedf file.

    Parameters
    ----------

    direc : string
        Directory of the sinogram
 
    prefix : string
        Prefix of the name of the sinogram

    scan_index : int
        Index of the scan

    verbose : boolean, default=False
        
    Outputs
    -------
    i : ndarray of float
        Sinogram
 
    Example
    -------
    >>> ????

    """

        
    #Projections
    #sys.stdout.write("\nRead sinogram ....")
    file_im = "%s/%s%.04d.edf" %(direc,prefix,scan_index);
    projec = EdfFile(file_im)
    hd = projec.GetStaticHeader(0) 

    dim1 = int(hd['Dim_1'])
    dim2 = int(hd['Dim_2'])
    i = np.zeros((dim2,dim1))
    i = projec.GetData(0)
    #sys.stdout.write("\rRead sinogram .... %d/%d" %(1,1))

    #Closure of edf projection file
    del projec

    #print "\n\n---> All data successfully imported.\n"

    return i 

