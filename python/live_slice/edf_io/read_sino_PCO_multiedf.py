import numpy as np
from os.path import exists
from live_slice.edf_io.EdfFile import *
from pylab import *
import re
import os
import sys

def read_sino_PCO_multiedf(direc, prefix, scan_index, slice=-1, noref=False, verbose=False):
    """
    Read a sinogramm, the flat and the dark from multiedf files.

    Parameters
    ----------

    direc : string
        Directory of the scan
 
    prefix : string
        Prefix of the name of the scan

    scan_index : int
        Index of the scan

    slice : int, default -1
        Index of the slice to read
        If slice=-1, read mid-height slice

    noref : boolean, default=False
        noref=False : look to find dark and flat    
        noref=True : skip dark and flat

    verbose : boolean, default=False
        
    Outputs
    -------
    dd : ndarray of float
        Median value of the dark

    ff : ndarray of float
        Median value of the flat
    
    pp : ndarray of float
        Sinogram
 
    Example
    -------
    >>> ????

    """
    #Projections
    sys.stdout.write("\nRead projections ....")
    file_im = "%s/%s%.04d.edf" %(direc,prefix,scan_index);
    if(verbose):
        print file_im
    projec = EdfFile(file_im)
    hd = projec.GetStaticHeader(0) 

    dim1 = int(hd['Dim_1'])
    dim2 = int(hd['Dim_2'])
    datatype=hd['DataType']
    print "datatype :" , datatype
    nproj = projec.GetNumImages()
    ii = np.zeros((1,dim2,nproj))

    if(slice<0):
        slice=dim1/2

    for i in range(0,nproj):
        ii[:,:,i] = projec.GetData(i,datatype,(0,slice),(dim2,1))
        sys.stdout.write("\rRead proj .... %d/%d" %(i+1,nproj))

    #Closure of edf projection file
    del projec

    ii=ii.squeeze()

    if(noref):
        d = np.zeros((1,dim2))
        f = 256*256*np.ones((1,dim2))
    else:
        #Define default values for flat and dark name
        fn_flat = "%s_ref_%.04d.edf" %(prefix,1)
        fn_dark = "%s_dark_%.04d.edf" %(prefix,1)


        #Check if a median has already been performed for trash and dark
        if exists("%s/dark.edf" %(direc)):
            fn_dark = 'dark.edf'
            ndark = 1

        if exists("%s/flat.edf"%(direc)):
            fn_flat = 'flat.edf'
            nflat = 1

        #Dark field
        sys.stdout.write("\nRead dark field ....")
        file_dark = "%s/%s" %(direc,fn_dark)
        if not exists("%s" %(file_dark)):
            reg1=re.compile(".*DARK.*.edf")
            reg2=re.compile(".*Dark.*.edf")
            reg3=re.compile(".*dark.*.edf")
            filelist=os.listdir(direc)
            file_dark_list=[m.group(0) for l in filelist for m in [reg1.search(l),reg2.search(l),reg3.search(l)] if m]        
            file_dark='%s/%s' %(direc,file_dark_list[0])
        if(verbose):
            print file_dark
        dark = EdfFile(file_dark)
        hd = dark.GetStaticHeader(0) 

        dim1 = int(hd['Dim_1'])
        dim2 = int(hd['Dim_2'])
        datatype=hd['DataType']
        ndark = dark.GetNumImages()
        d = np.zeros((1,dim2))

        if(ndark>1):
            dd = np.zeros((dim1,dim2,ndark))
            for i in range(0,ndark):
                dd[:,:,i] = dark.GetData(i)
            #d[0,:,i] = dd[slice,:,i]
                sys.stdout.write("\rRead dark field ... %d/%d" %(i+1,ndark) )
            del dark

            print "\nMedian dark fields ....\n"
            dd = np.median(dd,axis=2)
            d[0,:] = dd[slice,:]
            newdark = EdfFile("%s/dark.edf" %(direc))
            newdark.WriteImage (hd, dd, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
            del newdark
        else:
            d[:,:] = dark.GetData(0,datatype,(0,slice),(dim2,1))

        #Flat field
        sys.stdout.write("\nRead flat field ....")
        file_flat = "%s/%s" %(direc,fn_flat)
        if not exists("%s" %(file_flat)):
            reg1=re.compile(".*FLAT.*.edf")
            reg2=re.compile(".*Flat.*.edf")
            reg3=re.compile(".*flat.*.edf")
            reg4=re.compile(".*refHST.*.edf")
            reg5=re.compile(".*ref.*.edf")
            filelist=os.listdir(direc)
            file_flat_list=[m.group(0) for l in filelist for m in [reg1.search(l),reg2.search(l),reg3.search(l),reg4.search(l),reg5.search(l)] if m]        
            file_flat='%s/%s' %(direc,file_flat_list[0])
        if(verbose):
            print file_flat
        flat = EdfFile(file_flat)
        hd = flat.GetStaticHeader(0) 

        dim1 = int(hd['Dim_1'])
        dim2 = int(hd['Dim_2'])
        datatype=hd['DataType']
        nflat = flat.GetNumImages()
        f = np.zeros((1,dim2))
        
        if(nflat>1):
            ff = np.zeros((dim1,dim2,nflat))
            for i in range(0,nflat):
                ff[:,:,i] = flat.GetData(i)
                sys.stdout.write("\rRead flat fields .... %d/%d" %(i+1,nflat))
            del flat

            print "\nMedian flat fields ....\n"
            ff = np.median(ff,axis=2)
            f[0,:]=ff[slice,:] 
            newflat = EdfFile("%s/flat.edf" %(direc))
            newflat.WriteImage (hd, ff, Append=0, DataType="UnsignedShort", ByteOrder="LowByteFirst")
            del newflat
        else:
            f[:,:] = flat.GetData(0,datatype,(0,slice),(dim2,1))

        f=f.squeeze()
        d=d.squeeze()

        f=abs(f-d)  

        if(verbose):
            print "\n Dark and flat size is ", f.shape
    
        print "\n\n---> All data successfully imported.\n"
    return (d, f, ii.T) 

