#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Created on Tue Feb 10 12:00:00 2015

@author: Jonas Dittmann, jdittmann@physik.uni-wuerzburg.de


CUDA FPB implementation
"""


from __future__ import division
from numpy import arange, array, dot, zeros, ones, empty, pi, sin, cos, tan, sqrt, int32, float32, empty_like, zeros_like, intp, float64, uint16, dtype
from numpy.linalg import norm

import pycuda.autoinit
import pycuda.driver as drv
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gpuarray


srcmod = SourceModule("""

#define sq(x) ((x)*(x))
#define CONSTANT_PI 3.141592654f

texture<float, 2> sino2Dtex;//texture 3D for cross section
__global__ void backprojector(const float* __restrict__ setup, const int nSetups, float* __restrict__ image2D, int imDim1, int imDim2, int detDim) {
  float srcX, srcY, detX, detY, edgX, edgY;
  float originX, originY;
  float x, y, psx, psy, k;
  int setupOffset;
  const int idx = threadIdx.x + blockIdx.x * blockDim.x;
  const int i = idx / imDim2;
  const int j = idx - i * imDim2;//single line in image is fixing j
  float kFl, kCl;
  float wFl, wCl;
  float accum = 0;
  if (idx < imDim1 * imDim2) {
    originX = 0.5 * (imDim2 - 1);
    originY = 0.5 * (imDim1 - 1);
    const float r2 = sq(min(originX, originY));
    
    x = j - originX;
    y = originY - i;
    
    if (x*x + y*y <= r2) {
    
      for (int s = 0; s < nSetups; s++) {
        setupOffset = s * 6;
        srcX = setup[0+setupOffset];
        srcY = setup[1+setupOffset];
        detX = setup[2+setupOffset];
        detY = setup[3+setupOffset];
        edgX = setup[4+setupOffset];
        edgY = setup[5+setupOffset];
        
        
        psx = x - srcX;
        psy = y - srcY;
      
        k = 0.5 * (detDim - 1) + (detY * psx - detX * psy + y * srcX - x * srcY) / (edgX * psy - edgY * psx);
      
        kFl = floor(k);
        kCl = ceil(k);
        if (kFl == kCl) { kCl += 1; }
        
        wCl = k - kFl;
        wFl = kCl - k;
        
        accum += wFl * tex2D(sino2Dtex, kFl, s) + wCl * tex2D(sino2Dtex, kCl, s);//own interpolation not the texture one
        
      }
    }
    image2D[idx] = accum;
  }
}
  
__global__ void filter(float* sinoFiltered, int detDim, int nProjections) {
                                                                                                                                                                                                      
  const int idx = (blockIdx.x * blockDim.x + threadIdx.x);
    
  const float scale = -2 / (1. * CONSTANT_PI * nProjections);

  float accum = 0;
  
  const int p = idx / detDim;
  const int k = idx - p * detDim;
  
  if ((k < detDim) && (p < nProjections)) {
    for (int i = 0; i < detDim; i++) {
      accum += scale / (4 * sq((i-k)) - 1)  * tex2D(sino2Dtex, (float)i, (float)p); // sinogram weighting and shepp logan filter
        
    }
    sinoFiltered[idx] = accum;
  }
}

__global__ void normalizesino(float* sino, float* dark, float* flat, float* sinonormed, int detDim) {
                                                                                                                                                                                                      
  const int idx = (blockIdx.x * blockDim.x + threadIdx.x);
    
  if (idx < detDim) {
      sinonormed[idx] = -logf((sino[idx]-dark[idx])/flat[idx]); 
  }
}

""", options=['-use_fast_math', '-O3'])

backprojector = srcmod.get_function('backprojector'); backprojector.set_cache_config(drv.func_cache.PREFER_L1);
sinogramfilter = srcmod.get_function('filter');
sinoTexRef = srcmod.get_texref('sino2Dtex');
sinonorm = srcmod.get_function('normalizesino');

def initsino(dark, flat, setups, marginScale=1.):
    """Copy dark and flat to device.

Parameters
----------
dark : numpy.ndarray
    Dark of shape (detector pixels,)

flat : numpy.ndarray
    Flat of shape (detector pixels,)

setups : numpy.ndarray
    Continous list of setups. A single setup constists of 6 float values in
    the following order:
    sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
    detectorPixelEdgeX, detectorPixelEdgeY´
    
Returns
-------
(dark_device,flat_device)
"""

    if dark.dtype != float32:
      raise TypeError('dark must be provided as float32')
    if len(dark.shape) != 1:
      raise TypeError('dark must be one-dimensional')
    if flat.dtype != float32:
      raise TypeError('flat must be provided as float32')
    if len(flat.shape) != 1:
      raise TypeError('flat be one-dimensional')
    if setups.dtype != float32:
      raise TypeError('setups must be provided as float32')
    if len(setups) != len(setups // 6):
      raise ValueError('setup lines does not match provided setups')
    detDim = dark.shape[0]
    imDim = detDim
    imDim = int(round(imDim * marginScale))
    setups = setups.copy()
    pixelSize = float32(getFittingPixelSize(setups, detDim))
    #pixelSize = float32(1.0)
    setupsD = gpuarray.to_gpu(setups / pixelSize)
    darkD = gpuarray.to_gpu(dark.ravel())
    flatD = gpuarray.to_gpu(flat.ravel())
    return (darkD, flatD, setupsD)	

def initsetup(setups, detDim):
    """Copy setup to device.

Parameters
----------

setups : numpy.ndarray
    Continous list of setups. A single setup constists of 6 float values in
    the following order:
    sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
    detectorPixelEdgeX, detectorPixelEdgeY´
    
Returns
-------
(setup_device,flat_device)
"""

    if setups.dtype != float32:
      raise TypeError('setups must be provided as float32')
    if len(setups) != len(setups // 6):
      raise ValueError('setup lines does not match provided setups')
    setups = setups.copy()
    pixelSize = float32(getFittingPixelSize(setups, detDim))
    setupsD = gpuarray.to_gpu(setups / pixelSize)
    return setupsD	


def FBPonboard(sino, setupsD, darkD, flatD, blocksize=128, marginScale=1., normalize=True, noFilter=False):
    """Filtered Backprojection for linear detector Fanbeam using CUDA.

Parameters
----------
sino : numpy.ndarray
    Sinogram of shape (number of projections, detector pixels)
setups : numpy.ndarray
    Continous list of setups. A single setup constists of 6 float values in
    the following order:
    sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
    detectorPixelEdgeX, detectorPixelEdgeY´
blocksize : int
    CUDA blocksize, defaults to 128.
marginScale : float
    
Returns
-------
numpy.ndarray
    Reconstructed image of shape (number of detector pixels, number of detector pixels)
"""
    if sino.dtype != float32:
      raise TypeError('sinogram must be provided as float32')
    if len(sino.shape) != 2:
      raise TypeError('sinogram must be two-dimensional')
    detDim = sino.shape[1]
    imDim = detDim
    imDim = int(round(imDim * marginScale))
    #setups = setups.copy()
    nProjections = sino.shape[0]
    #pixelSize = float32(getFittingPixelSize(setups, detDim))
    #setupsD = gpuarray.to_gpu(setups / pixelSize)
    imageH = zeros((imDim, imDim), dtype=float32)
    imageD = gpuarray.zeros(imDim**2, dtype=float32)
    sinonormH = empty_like(sino)
    sinoFiltered = empty_like(sino)
    sinoD = gpuarray.to_gpu(sino.ravel())
    sinonormD = gpuarray.zeros(sino.size, dtype=float32)
    sinoFilteredD = gpuarray.zeros(sino.size, dtype=float32)
    sinoTexRef.set_filter_mode(drv.filter_mode.POINT)
    sinoTexRef.set_address_mode(0, drv.address_mode.BORDER)
    sinoTexRef.set_address_mode(1, drv.address_mode.BORDER)
    if normalize:
        for i in xrange(len(sino)):  #faster for some reason..
            blockx=blocksize #min(detDim, blocksize)
            sinonorm(intp(sinoD.gpudata) + intp(4 * detDim * i), intp(darkD.gpudata), intp(flatD.gpudata), intp(sinonormD.gpudata) + intp(4 * detDim * i), int32(detDim), block=(blockx, 1, 1), grid=((detDim+blocksize-1) // blocksize, 1, 1))    
    else:
        sinonormD = sinoD
    sinonormD.get(sinonormH)
    if noFilter:
        #sinoFilteredD = gpuarray.to_gpu(sino.ravel())
        drv.matrix_to_texref(sinonormH, sinoTexRef, order='C')
        #  sinoFilteredD = sinonormD
    else:
        drv.matrix_to_texref(sinonormH, sinoTexRef, order='C')
        blockx=blocksize#min(detDim*nProjections, blocksize)
        sinogramfilter(sinoFilteredD.gpudata, int32(detDim), int32(nProjections), block=(blockx, 1, 1), grid=((detDim*nProjections+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
        sinoFilteredD.get(sinoFiltered)
        drv.matrix_to_texref(sinoFiltered, sinoTexRef, order='C')
    
    blockx=blocksize# min(imDim**2, blocksize)
    backprojector(setupsD.gpudata, int32(nProjections), imageD.gpudata, int32(imDim), int32(imDim), int32(detDim), block=(blockx , 1, 1), grid=((imDim**2+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
    imageD.get(imageH)
    return imageH

 

def FBP(sino, setups, blocksize=256, marginScale=1., noFilter=False):
    """Filtered Backprojection for linear detector Fanbeam using CUDA.

Parameters
----------
sino : numpy.ndarray
    Sinogram of shape (number of projections, detector pixels)
setups : numpy.ndarray
    Continous list of setups. A single setup constists of 6 float values in
    the following order:
    sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
    detectorPixelEdgeX, detectorPixelEdgeY´
blocksize : int
    CUDA blocksize, defaults to 128.
marginScale : float
    
Returns
-------
numpy.ndarray
    Reconstructed image of shape (number of detector pixels, number of detector pixels)
"""
    if sino.dtype != float32:
      raise TypeError('sinogram must be provided as float32')
    if len(sino.shape) != 2:
      raise TypeError('sinogram must be two-dimensional')
    if setups.dtype != float32:
      raise TypeError('setups must be provided as float32')
    if len(setups) != len(setups // 6):
      raise ValueError('sinogram lines does not match provided setups')
    detDim = sino.shape[1]
    imDim = detDim
    imDim = int(round(imDim * marginScale))
    setups = setups.copy()
    nProjections = sino.shape[0]
    pixelSize = float32(getFittingPixelSize(setups, detDim))
    setupsD = gpuarray.to_gpu(setups / pixelSize)
    imageH = zeros((imDim, imDim), dtype=float32)
    imageD = gpuarray.zeros(imDim**2, dtype=float32)
    sinoFiltered = empty_like(sino)
    sinoD = gpuarray.to_gpu(sino.ravel())
    sinoFilteredD = gpuarray.zeros(sino.size, dtype=float32)
    sinoTexRef.set_filter_mode(drv.filter_mode.POINT)
    sinoTexRef.set_address_mode(0, drv.address_mode.BORDER)
    sinoTexRef.set_address_mode(1, drv.address_mode.BORDER)
    if noFilter:
      drv.matrix_to_texref(sino, sinoTexRef, order='C')

    else:
      drv.matrix_to_texref(sino, sinoTexRef, order='C')
      sinogramfilter(sinoFilteredD.gpudata, int32(detDim), int32(nProjections), block=(min(detDim*nProjections, blocksize), 1, 1), grid=((detDim*nProjections+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
      sinoFilteredD.get(sinoFiltered)
      drv.matrix_to_texref(sinoFiltered, sinoTexRef, order='C')
               
    backprojector(setupsD.gpudata, int32(nProjections), imageD.gpudata, int32(imDim), int32(imDim), int32(detDim), block=(min(imDim**2, blocksize), 1, 1), grid=((imDim**2+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
    imageD.get(imageH)
    return imageH


def rotation_op(theta):
  """Returns a 2x2 matrix for ccw rotation of a two dimensional vector.

Parameters
----------
theta : float
    angle in radians.

Returns
-------
numpy.ndarray
    2 x 2 rotation matrix
"""
  return array([ [cos(theta), -sin(theta)], [sin(theta), cos(theta)] ])
  
def rotate(vector, theta):
  """Rotates a two-dimensional vector ccw by angle theta (radian units)

Parameters
----------
vector : numpy.ndarray
    two element 1D vector to be rotated.
angle : float
    angle in radian units.
"""
  return dot(rotation_op(theta), vector)


def getFanbeamCTsetupsFromGeometry(sourceDist, detectorDist, detectorSize, detectorPixels, maxAngle, nProjections, angleOffset=0., detectorShift=0., axisShift=0., angleList=None):
  """Generates a setup description to be used by getSino(), cudaSART() and csreko(). Generates
axial CT setups from the given source/detector distances and detector extent.

Parameters
----------
sourceDist : float
    Source distance from object center in arbitrary units.
detectorDist : float
    Distance between detector center and object center in arbitrary units.
detectorSize : float
    Width of detector in arbitrary units.
detectorPixels : int
    Number of (equally sized) detector pixels.
maxAngle : float
    Angular range in radians over which the source/detector orientations will be distributed.
nProjections : int
    Number of source/detector setups
angleOffst : float
    Angle of the first setup in radians, defaults to 0.
detectorShift : float or array/list
    Displacement of detector center w.r.t. axis in units of pixels. Defaults to 0.
axisShift : float or array/list
    Displacement of axis perpendicular to the source-detector connection line. Defaults to 0.
angleList : array or list
    Absolute angles for each projection setup. maxAngle and nProjections will be ignored if
    angleList != None. Defaults to None

Returns
-------
numpy.ndarray
    list of setups to be used with getSino(), cudaSART() and csreko()
"""

  if angleList == None:
    angleList = maxAngle/nProjections * arange(nProjections) + angleOffset
  else:
    nProjections = len(angleList)
    
  try:
    if len(detectorShift) != nProjections:
      raise IndexError('detectorShift must be either scalar or a list/array of length nProjections or len(angleList)')
    else:
      detectorShifts = detectorShift
  except TypeError: # assume scalar is provided
    detectorShifts = ones(nProjections, dtype=float32) * detectorShift

  try:
    if len(axisShift) != nProjections:
      raise IndexError('axisShift must be either scalar or a list/array of length nProjections or len(angleList)')
    else:
      axisShifts = axisShift
  except TypeError: # assume scalar is provided
    axisShifts = ones(nProjections, dtype=float32) * axisShift    
    
  setups = empty(6 * nProjections, dtype=float32)
  
  for i in xrange(nProjections):
    k = i * 6
    #setups[k  :k+2] = rotate(array((-sourceDist, 0.)), maxAngle/nProjections * i + angleOffset)
    #det = rotate(array((detectorDist, 0.)), maxAngle/nProjections * i + angleOffset)
    setups[k  :k+2] = rotate(array((-sourceDist, 0.)), angleList[i])
    det = rotate(array((detectorDist, 0.)), angleList[i])
    setups[k+2:k+4] = det
    setups[k+4:k+6] = array((-det[1], det[0])) / norm(det) * detectorSize / detectorPixels
    setups[k+2:k+4] +=  setups[k+4:k+6] * detectorShifts[i]
    setups[k+2:k+4] -=  setups[k+4:k+6] * axisShifts[i]
    setups[k+0:k+2] -=  setups[k+4:k+6] * axisShifts[i]
  return setups


def getFanbeamCTsetupsFromOpeningAngle(openingAngleDeg, detectorPixels, maxAngle, nProjections, angleOffset=0., detectorSize=None, detectorShift=0., axisShift=0., angleList=None):
  """Generates a setup description to be used by getSino(), cudaSART() and csreko(). Generates
axial CT setups from the given beam divergence (and optional detector size).

Parameters
----------
openingAngleDeg : float
    Beam divergence in degrees.
detectorPixels : int
    Number of detectorPixels.
maxAngle : float
    Angular range in radians over which the source/detector orientations will be distributed.
nProjetions : int
    Number of generated setups.
angleOffset : float
    Orientation of the first setup in radians, defaults to 0.
detectorSize : float
    Size of the detector in arbitrary units, defaults to nProjections.
detectorShift : float or array/list
    Displacement of detector center w.r.t. axis in units of pixels. Defaults to 0.
axisShift : float or array/list
    Displacement of axis perpendicular to the source-detector connection line. Defaults to 0.
angleList : array or list
    Absolute angles for each projection setup. maxAngle and nProjections will be ignored if
    angleList != None. Defaults to None

Returns
-------
numpy.ndarray
    List of setups to be used with getSino(), cudaSART() and csreko()
"""
  if detectorSize == None:
    detectorSize = detectorPixels
  d = 1./tan(openingAngleDeg / 180. * pi * 0.5) * 0.25 * detectorSize
  return getFanbeamCTsetupsFromGeometry(d, d, detectorSize, detectorPixels, maxAngle, nProjections, angleOffset, detectorShift, axisShift, angleList)

def getFittingPixelSize(setup, detPixels):          # TODO: currently only sensible if source, sample and detector line up and detector normal is parralel to that line
  detectorSize           = norm(setup[4:6]) * detPixels
  sourceDetectorDistance = norm(setup[2:4] - setup[0:2])
  sourceDistance         = norm(setup[0:2])
  x = detectorSize / (2 * sourceDetectorDistance)
  sampleSize = 2 * x / sqrt(x**2 + 1) * sourceDistance  # a
  return sampleSize / detPixels


  
  
