#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import os
from os.path import exists
import sys
import re
from numpy import arange, array, dot, zeros, ones, empty, pi, sin, cos, tan, sqrt, int32, float32, empty_like, zeros_like, intp, float64, uint16, dtype
from numpy.linalg import norm

pv = sys.version_info
python_version = pv[0:2]
if (python_version >= (2,7)):
    from scipy.optimize import curve_fit

from live_slice.edf_io.EdfFile import *
from live_slice.edf_io.RawFile import *
from live_slice.edf_io.read_sino_PCO_singleedf import read_sino_PCO_singleedf
from live_slice.edf_io.read_sino_PCO_singleedf import read_more_sino_PCO_singleedf
from live_slice.edf_io.read_sino_PCO_multiedf import read_sino_PCO_multiedf
#from live_slice.pyhstlink.make_slice_par import make_slice_par
#from live_slice.pyhstlink.run_pyhst import run_pyhst
#from live_slice.imagejlink.run_imagej_live_slice import run_imagej_live_slice
#from live_slice.imagejlink.run_imagej_select_slice import run_imagej_select_slice
#from live_slice.imagejlink.kill_imagej import kill_imagej
#from live_slice.FBP.CudaFBP_02_2015 import *
import time
    
from time import clock
from PyQt4.QtCore import *
from PyQt4.QtGui import *
try:
    import pyqtgraph
    use_pyqtgraph = True
    print 'use pyqtgraph'
except ImportError:
    use_pyqtgraph = False
    from matplotlib import pyplot as plt
    from matplotlib.figure import Figure
    from matplotlib.backends.backend_qt4agg import (
        FigureCanvasQTAgg as FigureCanvas,
        NavigationToolbar2QT as NavigationToolbar)

try:
    import pycuda.driver as cuda
    cuda.init()
#    #print 'cuda initiated'
except ImportError:
    pass

about_message = 'Live CT Reconstruction and Viewer\n\n' \
                'Developed by LRM/Universitat Wurzburg (Germany) and SIMaP/University Grneoble Alpes (France):\n' \
                'Jonas Dittmann (CT reconstruction, LRM) and\n'\
                'Maximilian Ullherr (Initial user interface, LRM) and\n' \
                'Pierre Lhuissier (Integration on beamline and evolution, SIMaP) \n\n' \
                'A simple cone beam reconstruction from a sinogram file, automatic update for live view ' \
                'triggers a new reconstruction. The axis shift can be changed and the reconstruction will be ' \
                'rerun automatically.\n\n' \
                'Uses pyqtgraph or matplotlib for the image view.' \
                '\n\n' \
                'pierre.lhuissier@simap.grenoble-inp.fr, 06. Jul. 2015'

BOX_SCROLL_SPEED = 1.2  # one step is (val - 1) of the original value
SHOW_SLICE_SELECTOR = False
SHOW_ANGLE_SELECTOR = False
SHOW_AXIS_SELECTOR = True
SHOW_AXIS_MANAGER_SELECTOR = False
SHOW_THRESHOLD_SELECTOR = True
SHOW_PATH_SELECTOR = True
SHOW_SCAN_SELECTOR = True


#############################################################################


class Reconstructor(QThread):
    reconstructionFinished = pyqtSignal()
    initiationFinished = pyqtSignal()
    startReconstruction    = pyqtSignal()
    initReconstruction    = pyqtSignal()
    def __init__(self, sino_path='', parent=None):
        QThread.__init__(self, parent)
        self.isBusy = False
        self.reconstructed = None
        self.sinogram = None
        self.sinogram_is_new = True
        self.angle_file = None
        self.angle_range = None
        self.axis_shift = 0.
        self.threshold_min = 0.
        self.threshold_max = 1.
        self.current_scan = 0
        self.last_scan = -1
        self.axis_shift_locked = False
        self.hasSino=False
        self.sino_path = sino_path

    def set_axis_shift(self, axis_shift):
        self.axis_shift = axis_shift

    def set_min_threshold(self, threshold):
        self.min_threshold = threshold
    
    def set_max_threshold(self, threshold):
        self.max_threshold = threshold
    
    def set_current_scan(self, current):
        self.current_scan = current
    
    def set_last_scan(self, last):
        self.last_scan = last
        
    def set_new_sinogram(self, sinogram, sinogram_is_new=True):
        self.sinogram = sinogram
        self.sinogram_is_new = sinogram_is_new
        self.hasSino=True

    def run(self):  # only code in here is run threaded!
        self.dev = cuda.Device(0)
        self.ctx = self.dev.make_context()
        #from live_slice.FBP.CudaFBP_09_2015 import getFanbeamCTsetupsFromOpeningAngle, FBPonboard, initsetup, initsino
        #import pycuda.autoinit
        import pycuda.driver as drv
        from pycuda.compiler import SourceModule
        import pycuda.gpuarray as gpuarray

        srcmod = SourceModule("""
        #define sq(x) ((x)*(x))
        #define CONSTANT_PI 3.141592654f

        texture<float, 2> sino2Dtex;//texture 3D for cross section
        __global__ void backprojector(const float* __restrict__ setup, const int nSetups, float* __restrict__ image2D, int imDim1, int imDim2, int detDim) {
                float srcX, srcY, detX, detY, edgX, edgY;
                float originX, originY;
                float x, y, psx, psy, k;
                int setupOffset;
                const int idx = threadIdx.x + blockIdx.x * blockDim.x;
                const int i = idx / imDim2;
                const int j = idx - i * imDim2;//single line in image is fixing j
                float kFl, kCl;
                float wFl, wCl;
                float accum = 0;
                if (idx < imDim1 * imDim2) {
                originX = 0.5 * (imDim2 - 1);
                originY = 0.5 * (imDim1 - 1);
                const float r2 = sq(min(originX, originY));

                x = j - originX;
                y = originY - i;

                if (x*x + y*y <= r2) {

                    for (int s = 0; s < nSetups; s++) {
                        setupOffset = s * 6;
                        srcX = setup[0+setupOffset];
                        srcY = setup[1+setupOffset];
                        detX = setup[2+setupOffset];
                        detY = setup[3+setupOffset];
                        edgX = setup[4+setupOffset];
                        edgY = setup[5+setupOffset];


                        psx = x - srcX;
                        psy = y - srcY;

                        k = 0.5 * (detDim - 1) + (detY * psx - detX * psy + y * srcX - x * srcY) / (edgX * psy - edgY * psx);

                        kFl = floor(k);
                        kCl = ceil(k);
                        if (kFl == kCl) { kCl += 1; }

                        wCl = k - kFl;
                        wFl = kCl - k;

                        accum += wFl * tex2D(sino2Dtex, kFl, s) + wCl * tex2D(sino2Dtex, kCl, s);//own interpolation not the texture one

                    }
                }
                image2D[idx] = accum;
                }
                }

        __global__ void filter(float* sinoFiltered, int detDim, int nProjections) {

            const int idx = (blockIdx.x * blockDim.x + threadIdx.x);
            const float scale = -2 / (1. * CONSTANT_PI * nProjections);

            float accum = 0;

            const int p = idx / detDim;
            const int k = idx - p * detDim;

            if ((k < detDim) && (p < nProjections)) {
                for (int i = 0; i < detDim; i++) {
                    accum += scale / (4 * sq((i-k)) - 1)  * tex2D(sino2Dtex, (float)i, (float)p); // sinogram weighting and shepp logan filter

                }
                sinoFiltered[idx] = accum;
            }
        }

        __global__ void normalizesino(float* sino, float* dark, float* flat, float* sinonormed, int detDim) {

            const int idx = (blockIdx.x * blockDim.x + threadIdx.x);

            if (idx < detDim) {
                sinonormed[idx] = -logf((sino[idx]-dark[idx])/flat[idx]); 
            }
        }
        """, options=['-use_fast_math', '-O3'])

        backprojector = srcmod.get_function('backprojector'); backprojector.set_cache_config(drv.func_cache.PREFER_L1);
        sinogramfilter = srcmod.get_function('filter');
        sinoTexRef = srcmod.get_texref('sino2Dtex');
        sinonorm = srcmod.get_function('normalizesino');

        def initsino(dark, flat, setups, marginScale=1.):
            """Copy dark and flat to device.

            Parameters
            ----------
            dark : numpy.ndarray
                Dark of shape (detector pixels,)

            flat : numpy.ndarray
                Flat of shape (detector pixels,)

            setups : numpy.ndarray
                Continous list of setups. A single setup constists of 6 float values in
                the following order:
                sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
                detectorPixelEdgeX, detectorPixelEdgeY´
            
            Returns
            -------
            (dark_device,flat_device)
            """

            if dark.dtype != float32:
              raise TypeError('dark must be provided as float32')
            if len(dark.shape) != 1:
              raise TypeError('dark must be one-dimensional')
            if flat.dtype != float32:
              raise TypeError('flat must be provided as float32')
            if len(flat.shape) != 1:
              raise TypeError('flat be one-dimensional')
            if setups.dtype != float32:
              raise TypeError('setups must be provided as float32')
            if len(setups) != len(setups // 6):
              raise ValueError('setup lines does not match provided setups')
            detDim = dark.shape[0]
            imDim = detDim
            imDim = int(round(imDim * marginScale))
            setups = setups.copy()
            pixelSize = float32(getFittingPixelSize(setups, detDim))
            #pixelSize = float32(1.0)
            setupsD = gpuarray.to_gpu(setups / pixelSize)
            darkD = gpuarray.to_gpu(dark.ravel())
            flatD = gpuarray.to_gpu(flat.ravel())
            return (darkD, flatD, setupsD)	

        def initsetup(setups, detDim):
            """Copy setup to device.

            Parameters
            ----------

            setups : numpy.ndarray
                Continous list of setups. A single setup constists of 6 float values in
                the following order:
                sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
                detectorPixelEdgeX, detectorPixelEdgeY´
            
            Returns
            -------
            (setup_device,flat_device)
            """

            if setups.dtype != float32:
              raise TypeError('setups must be provided as float32')
            if len(setups) != len(setups // 6):
              raise ValueError('setup lines does not match provided setups')
            setups = setups.copy()
            pixelSize = float32(getFittingPixelSize(setups, detDim))
            setupsD = gpuarray.to_gpu(setups / pixelSize)
            return setupsD	


        def FBPonboard(sino, setupsD, darkD, flatD, blocksize=128, marginScale=1., normalize=True, noFilter=False):
            """Filtered Backprojection for linear detector Fanbeam using CUDA.

            Parameters
            ----------
            sino : numpy.ndarray
                Sinogram of shape (number of projections, detector pixels)
            setups : numpy.ndarray
                Continous list of setups. A single setup constists of 6 float values in
                the following order:
                sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
                detectorPixelEdgeX, detectorPixelEdgeY´
            blocksize : int
                CUDA blocksize, defaults to 128.
            marginScale : float     

            Returns
            -------
            numpy.ndarray
                Reconstructed image of shape (number of detector pixels, number of detector pixels)
            """
            if sino.dtype != float32:
              raise TypeError('sinogram must be provided as float32')
            if len(sino.shape) != 2:
              raise TypeError('sinogram must be two-dimensional')
            detDim = sino.shape[1]
            imDim = detDim
            imDim = int(round(imDim * marginScale))
            #setups = setups.copy()
            nProjections = sino.shape[0]
            #pixelSize = float32(getFittingPixelSize(setups, detDim))
            #setupsD = gpuarray.to_gpu(setups / pixelSize)
            imageH = zeros((imDim, imDim), dtype=float32)
            imageD = gpuarray.zeros(imDim**2, dtype=float32)
            sinonormH = empty_like(sino)
            sinoFiltered = empty_like(sino)
            sinoD = gpuarray.to_gpu(sino.ravel())
            sinonormD = gpuarray.zeros(sino.size, dtype=float32)
            sinoFilteredD = gpuarray.zeros(sino.size, dtype=float32)
            sinoTexRef.set_filter_mode(drv.filter_mode.POINT)
            sinoTexRef.set_address_mode(0, drv.address_mode.BORDER)
            sinoTexRef.set_address_mode(1, drv.address_mode.BORDER)
            if normalize:
                for i in xrange(len(sino)):  #faster for some reason..
                    blockx=blocksize #min(detDim, blocksize)
                    sinonorm(intp(sinoD.gpudata) + intp(4 * detDim * i), intp(darkD.gpudata), intp(flatD.gpudata), intp(sinonormD.gpudata) + intp(4 * detDim * i), int32(detDim), block=(blockx, 1, 1), grid=((detDim+blocksize-1) // blocksize, 1, 1))    
            else:
                sinonormD = sinoD
            sinonormD.get(sinonormH)
            if noFilter:
                #sinoFilteredD = gpuarray.to_gpu(sino.ravel())
                drv.matrix_to_texref(sinonormH, sinoTexRef, order='C')
                #  sinoFilteredD = sinonormD
            else:
                drv.matrix_to_texref(sinonormH, sinoTexRef, order='C')
                blockx=blocksize#min(detDim*nProjections, blocksize)
                sinogramfilter(sinoFilteredD.gpudata, int32(detDim), int32(nProjections), block=(blockx, 1, 1), grid=((detDim*nProjections+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
                sinoFilteredD.get(sinoFiltered)
                drv.matrix_to_texref(sinoFiltered, sinoTexRef, order='C')
            
            blockx=blocksize# min(imDim**2, blocksize)
            backprojector(setupsD.gpudata, int32(nProjections), imageD.gpudata, int32(imDim), int32(imDim), int32(detDim), block=(blockx , 1, 1), grid=((imDim**2+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
            imageD.get(imageH)
            return imageH

         

        def FBP(sino, setups, blocksize=256, marginScale=1., noFilter=False):
            """Filtered Backprojection for linear detector Fanbeam using CUDA.

            Parameters
            ----------
            sino : numpy.ndarray
                Sinogram of shape (number of projections, detector pixels)
            setups : numpy.ndarray
                Continous list of setups. A single setup constists of 6 float values in
                the following order:
                sourcePositionX, sourcePositionY, detectorPositionX, detectorPositionY, 
                detectorPixelEdgeX, detectorPixelEdgeY´
            blocksize : int
                CUDA blocksize, defaults to 128.
            marginScale : float
            
            Returns
            -------
            numpy.ndarray
                Reconstructed image of shape (number of detector pixels, number of detector pixels)
            """
            if sino.dtype != float32:
              raise TypeError('sinogram must be provided as float32')
            if len(sino.shape) != 2:
              raise TypeError('sinogram must be two-dimensional')
            if setups.dtype != float32:
              raise TypeError('setups must be provided as float32')
            if len(setups) != len(setups // 6):
              raise ValueError('sinogram lines does not match provided setups')
            detDim = sino.shape[1]
            imDim = detDim
            imDim = int(round(imDim * marginScale))
            setups = setups.copy()
            nProjections = sino.shape[0]
            pixelSize = float32(getFittingPixelSize(setups, detDim))
            setupsD = gpuarray.to_gpu(setups / pixelSize)
            imageH = zeros((imDim, imDim), dtype=float32)
            imageD = gpuarray.zeros(imDim**2, dtype=float32)
            sinoFiltered = empty_like(sino)
            sinoD = gpuarray.to_gpu(sino.ravel())
            sinoFilteredD = gpuarray.zeros(sino.size, dtype=float32)
            sinoTexRef.set_filter_mode(drv.filter_mode.POINT)
            sinoTexRef.set_address_mode(0, drv.address_mode.BORDER)
            sinoTexRef.set_address_mode(1, drv.address_mode.BORDER)
            if noFilter:
              drv.matrix_to_texref(sino, sinoTexRef, order='C')

            else:
              drv.matrix_to_texref(sino, sinoTexRef, order='C')
              sinogramfilter(sinoFilteredD.gpudata, int32(detDim), int32(nProjections), block=(min(detDim*nProjections, blocksize), 1, 1), grid=((detDim*nProjections+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
              sinoFilteredD.get(sinoFiltered)
              drv.matrix_to_texref(sinoFiltered, sinoTexRef, order='C')
                       
            backprojector(setupsD.gpudata, int32(nProjections), imageD.gpudata, int32(imDim), int32(imDim), int32(detDim), block=(min(imDim**2, blocksize), 1, 1), grid=((imDim**2+blocksize-1) // blocksize, 1, 1), texrefs=[sinoTexRef])
            imageD.get(imageH)
            return imageH


        def rotation_op(theta):
            """Returns a 2x2 matrix for ccw rotation of a two dimensional vector.

            Parameters
            ----------
            theta : float
                angle in radians.

            Returns
            -------
            numpy.ndarray
                2 x 2 rotation matrix
            """
            return array([ [cos(theta), -sin(theta)], [sin(theta), cos(theta)] ])
          
        def rotate(vector, theta):
            """Rotates a two-dimensional vector ccw by angle theta (radian units)

            Parameters
            ----------
            vector : numpy.ndarray
                two element 1D vector to be rotated.
            angle : float
                angle in radian units.
            """
            return dot(rotation_op(theta), vector)


        def getFanbeamCTsetupsFromGeometry(sourceDist, detectorDist, detectorSize, detectorPixels, maxAngle, nProjections, angleOffset=0., detectorShift=0., axisShift=0., angleList=None):
            """Generates a setup description to be used by getSino(), cudaSART() and csreko(). Generates
            axial CT setups from the given source/detector distances and detector extent.

            Parameters
            ----------
            sourceDist : float
                Source distance from object center in arbitrary units.
            detectorDist : float
                Distance between detector center and object center in arbitrary units.
            detectorSize : float
                Width of detector in arbitrary units.
            detectorPixels : int
                Number of (equally sized) detector pixels.
            maxAngle : float
                Angular range in radians over which the source/detector orientations will be distributed.
            nProjections : int
                Number of source/detector setups
            angleOffst : float
                Angle of the first setup in radians, defaults to 0.
            detectorShift : float or array/list
                Displacement of detector center w.r.t. axis in units of pixels. Defaults to 0.
            axisShift : float or array/list
                Displacement of axis perpendicular to the source-detector connection line. Defaults to 0.
            angleList : array or list
                Absolute angles for each projection setup. maxAngle and nProjections will be ignored if
                angleList != None. Defaults to None

            Returns
            -------
            numpy.ndarray
                list of setups to be used with getSino(), cudaSART() and csreko()
            """

            if angleList == None:
                angleList = maxAngle/nProjections * arange(nProjections) + angleOffset
            else:
                nProjections = len(angleList)
                
            try:
                if len(detectorShift) != nProjections:
                  raise IndexError('detectorShift must be either scalar or a list/array of length nProjections or len(angleList)')
                else:
                  detectorShifts = detectorShift
            except TypeError: # assume scalar is provided
                detectorShifts = ones(nProjections, dtype=float32) * detectorShift

            try:
                if len(axisShift) != nProjections:
                  raise IndexError('axisShift must be either scalar or a list/array of length nProjections or len(angleList)')
                else:
                  axisShifts = axisShift
            except TypeError: # assume scalar is provided
                axisShifts = ones(nProjections, dtype=float32) * axisShift    
                
            setups = empty(6 * nProjections, dtype=float32)
              
            for i in xrange(nProjections):
                k = i * 6
                #setups[k  :k+2] = rotate(array((-sourceDist, 0.)), maxAngle/nProjections * i + angleOffset)
                #det = rotate(array((detectorDist, 0.)), maxAngle/nProjections * i + angleOffset)
                setups[k  :k+2] = rotate(array((-sourceDist, 0.)), angleList[i])
                det = rotate(array((detectorDist, 0.)), angleList[i])
                setups[k+2:k+4] = det
                setups[k+4:k+6] = array((-det[1], det[0])) / norm(det) * detectorSize / detectorPixels
                setups[k+2:k+4] +=  setups[k+4:k+6] * detectorShifts[i]
                setups[k+2:k+4] -=  setups[k+4:k+6] * axisShifts[i]
                setups[k+0:k+2] -=  setups[k+4:k+6] * axisShifts[i]
            return setups

        def getFanbeamCTsetupsFromOpeningAngle(openingAngleDeg, detectorPixels, maxAngle, nProjections, angleOffset=0., detectorSize=None, detectorShift=0., axisShift=0., angleList=None):
            """Generates a setup description to be used by getSino(), cudaSART() and csreko(). Generates
            axial CT setups from the given beam divergence (and optional detector size).

            Parameters
            ----------
            openingAngleDeg : float
                Beam divergence in degrees.
            detectorPixels : int
                Number of detectorPixels.
            maxAngle : float
                Angular range in radians over which the source/detector orientations will be distributed.
            nProjetions : int
                Number of generated setups.
            angleOffset : float
                Orientation of the first setup in radians, defaults to 0.
            detectorSize : float
                Size of the detector in arbitrary units, defaults to nProjections.
            detectorShift : float or array/list
                Displacement of detector center w.r.t. axis in units of pixels. Defaults to 0.
            axisShift : float or array/list
                Displacement of axis perpendicular to the source-detector connection line. Defaults to 0.
            angleList : array or list
                Absolute angles for each projection setup. maxAngle and nProjections will be ignored if
                angleList != None. Defaults to None

            Returns
            -------
            numpy.ndarray
                List of setups to be used with getSino(), cudaSART() and csreko()
            """
            if detectorSize == None:
                detectorSize = detectorPixels
            d = 1./tan(openingAngleDeg / 180. * pi * 0.5) * 0.25 * detectorSize
            return getFanbeamCTsetupsFromGeometry(d, d, detectorSize, detectorPixels, maxAngle, nProjections, angleOffset, detectorShift, axisShift, angleList)

        def getFittingPixelSize(setup, detPixels): # TODO: currently only sensible if source, sample and detector line up and detector normal is parralel to that line
            detectorSize           = norm(setup[4:6]) * detPixels
            sourceDetectorDistance = norm(setup[2:4] - setup[0:2])
            sourceDistance         = norm(setup[0:2])
            x = detectorSize / (2 * sourceDetectorDistance)
     	    sampleSize = 2 * x / sqrt(x**2 + 1) * sourceDistance
            return sampleSize / detPixels

        def initiate():
            print 'init gpu here'
            sino_path=self.sino_path
            print 'sino_path = %s' %(sino_path)
            (direc, sep, name) = sino_path.rpartition('/')
            (fullprefix, sep, suffix) = name.rpartition('.')
            #self.direc=os.getcwd()
            self.direc=direc
            self.direc=self.direc.replace('mntdirect/_','')
            self.direcrec="%s/rec" %(direc)
            if( not exists(self.direcrec)): 
                os.mkdir(self.direcrec)
            index_str = re.findall('\d+',fullprefix)[-1] 
            prefix = name.replace('%s.%s' %(index_str,suffix),'')
            self.current_scan = int(index_str)
            #self.prefix_scan = "%s%.04d" %(self.prefix,scan_index)
            file_size = 0
            if(exists("%s/%s%.04d.edf" %(self.direc,prefix,self.current_scan))):
                [ d , f , i ] = read_sino_PCO_singleedf(self.direc,prefix,self.current_scan,True,False)
                self.dim1 = i.shape[0]
                self.dim2 = i.shape[1]
                darkfloat = np.float32(d)
                flatfloat = np.float32(f)
                self.sinogram=np.float32(i)
                setups = getFanbeamCTsetupsFromOpeningAngle(0.01, self.dim2, pi, self.dim1, axisShift=0.)
                (self.darkD,self.flatD, setupsD) = initsino(darkfloat,flatfloat,setups)
                #self.darkD = darkD
                #self.flatD = flatD
                file_size = self.dim1*self.dim2*2+1024
            self.file_size = file_size
            self.initiationFinished.emit()

        def reconstruct():
            ts1 = clock()
            self.isBusy = True
            #if self.sinogram_is_new:
            #    self.sinogram = sinogramFilter(self.sinogram)
            #    self.sinogram_is_new = False
            sino = self.sinogram
            max_angle = (self.angle_range[1]-self.angle_range[0]) / 180. * np.pi
            angle_offset = (self.angle_range[0]) / 180. * np.pi
            nProjections   = sino.shape[0]
            detDim         = sino.shape[1]
            setups         = getFanbeamCTsetupsFromOpeningAngle(0.01, detDim, max_angle, nProjections, detectorShift=self.axis_shift, angleOffset=angle_offset)
            try: 
              setupsD = initsetup(setups,detDim)
              print 'reconstruct here'
              print 'device'
              print self.dev
              print self.ctx
              reconstructed = FBPonboard(sino, setupsD, self.darkD, self.flatD)
              #writeRaw("%s/%s%.04d.vol" %(self.direcrec,self.prefix,self.scan_index),reconstructed)
            except:
              self.ctx.pop()
              del self.ctx
              raise
            
            self.reconstructed = reconstructed
            # self.reconstructed[self.current_scan] = reconstructed
            self.isBusy = False
            self.reconstructionFinished.emit()
        self.startReconstruction.connect(reconstruct)
        self.initReconstruction.connect(initiate)
        self.finished.connect(self.clear)
        self.exec_()

    def clear(self):
        self.ctx.pop()
        del self.ctx


class ScanChecker(QThread):
    newScanFound = pyqtSignal()

    def __init__(self,parent=None):
        QThread.__init__(self, parent)
        self.last_found_scan=-1
        self.looked_scan=-1
        self.isRunning=False

    def initiate(self, sino_path, file_size):
        (direc, sep, name) = sino_path.rpartition('/')
        (fullprefix, sep, suffix) = name.rpartition('.')
        self.direc=direc
        self.direc=self.direc.replace('mntdirect/_','')
        index_str = re.findall('\d+',fullprefix)[-1] 
        self.prefix = name.replace('%s.%s' %(index_str,suffix),'')
        self.looked_scan =  int(index_str)
        self.file_size = file_size

    def look_for_new_scan(self):
        print 'start auto update'
        self.start(QThread.LowPriority)

    def look_scan_from_file(self):
        waiting=True
        #print 'waiting for %s/%s%.04d.edf' %(self.direc,self.prefix,self.looked_scan)

        while(waiting):
            if(exists("%s/%s%.04d.edf" %(self.direc,self.prefix,self.looked_scan))):
                if(os.path.getsize("%s/%s%.04d.edf" %(self.direc,self.prefix,self.looked_scan))==self.file_size):
                    self.last_found_scan = self.looked_scan
                    self.looked_scan=self.looked_scan + 1
                    waiting=False

    def run(self):  # only code in here is run threaded!
        self.isRunning=True
        while(True):
            self.look_scan_from_file()  # needs to be defined
            self.newScanFound.emit()
            #print 'found new scan %d' %(self.last_found_scan)


class SinogramReader(QThread):
    sinogramFinished = pyqtSignal()

    def initiate(self, sino_path):
        (direc, sep, name) = sino_path.rpartition('/')
        print 'direc = %s' %(direc)
        (fullprefix, sep, suffix) = name.rpartition('.')
        self.direc=direc
        self.direc=self.direc.replace('mntdirect/_','')
        index_str = re.findall('\d+',fullprefix)[-1] 
        self.prefix = name.replace('%s.%s' %(index_str,suffix),'')
        self.current_scan = int(index_str)
        self.last_index = self.current_scan
        self.isBusy=False
        self.sinogram=None

    def set_last_sinogram(self, index):
        self.last_index=index
    
    def set_direc(self,direc):
        self.direc = direc

    def set_prefix(self,prefix):
        self.prefix=prefix

    def read_sinogram(self, index):
        if not self.isBusy:
            self.isBusy=True
            self.current_scan = index
            self.start(QThread.LowPriority)
        self.isBusy = False

    def read_last_sinogram(self):
        if not self.isBusy:
            self.isBusy=True
            self.current_scan = self.last_index
            print "start readding sinogramm %d " %(self.current_scan)
            self.start(QThread.LowPriority)
            print "finished readding sinogramm %d " %(self.current_scan)
        self.isBusy = False

    def read_sinogram_from_file(self):
        i = read_more_sino_PCO_singleedf(self.direc,self.prefix,self.current_scan,False)
        self.sinogram = np.float32(i)

    def run(self):  # only code in here is run threaded!
        self.read_sinogram_from_file()  # needs to be defined
        #self.sinogram = read_sinogram_from_file_ext(self.direc, self.prefix, self.current_scan)  # needs to be defined
        self.sinogramFinished.emit()

#def read_sinogram_from_file_ext(direc,prefix,current_scan):
#    i = read_more_sino_PCO_singleedf(direc,prefix,current_scan,False)
#    return np.float32(i)

def write_slice_info(self):
    pass


############################################################################

class ImageView(QWidget):
        indexChanged = pyqtSignal(int)
        def __init__(self, parent=None, autoscale_percentiles=(1, 99)):
            QWidget.__init__(self, parent)
            self.fig = Figure((4.0, 4.0), dpi=100)
            self.fig.subplots_adjust(0.01, 0.01, 0.99, 0.99)   #arguments: left, bottom, right, top
            #self.fig.set_tight_layout(True)
            self.axes = self.fig.add_subplot(111)
            self.canvas = FigureCanvas(self.fig)
            self.canvas.wheelEvent = lambda event: event.ignore()
            self.mpl_toolbar = NavigationToolbar(self.canvas, self)
            vbox = QVBoxLayout()
            vbox.addWidget(self.mpl_toolbar)
            vbox.addWidget(self.canvas)
            self.setLayout(vbox)

            self.autoscale_percentiles = autoscale_percentiles
            self.external_display_range = False
            self.colorbar = True
            self.enabled = False
            self.vmin, self.vmax = 0., 0.

        def set_scales(self, data, autoscale):
            if self.external_display_range:
                self.vmin, self.vmax = self.external_display_range
            else:
                if autoscale:
                    self.vmin = np.percentile(data[::4, ::4], self.autoscale_percentiles[0])
                    self.vmax = np.percentile(data[::4, ::4], self.autoscale_percentiles[1])
                else:
                    if data.dtype == 'bool':
                        self.vmin, self.vmax = 0, 1
                    else:
                        self.vmin, self.vmax = np.min(data), np.max(data)
            self.range_len = self.vmax - self.vmin

        def set_colorbar(self, ticks=5):
            pad = (self.vmax-self.vmin)/(2*ticks)
            labels = ['{:.2e}'.format(val) for val in np.linspace(self.vmin+pad, self.vmax-pad, ticks)]
            ticks = [val for val in np.linspace(self.vmin+pad, self.vmax-pad, ticks)]
            if not hasattr(self, 'cbar'):
                self.cbar = self.fig.colorbar(self.image_view, orientation='horizontal', fraction=0.09, pad=0.02,
                                              aspect=50)
            self.cbar.set_ticks(ticks)
            self.cbar.ax.set_xticklabels(labels)

        def enable(self, data, autoscale=True):
            if data is not None:
                self.axes.axis('off')
                self.autoscale = autoscale
                self.set_scales(data, autoscale)
                self.image_view = self.axes.imshow(data, cmap=plt.cm.gray, interpolation='None',
                                                   vmin=self.vmin, vmax=self.vmax)
                if self.colorbar: self.set_colorbar()
                self.enabled = True
                self.show_image(data)

        def show_image(self, data, rescale=True):
            if data is not None:
                if not self.enabled:
                    self.enable(data)
                else:
                    time0 = time.clock()
                    if rescale: self.set_scales(data, self.autoscale)
                    print('time for rescale: {:.3f}'.format(time.clock()-time0)); time0 = time.clock()
                    if self.colorbar: self.set_colorbar()
                    print('time for colorbar: {:.3f}'.format(time.clock()-time0)); time0 = time.clock()
                    self.image_view.set_data((data*255).astype('u1'))
                    print('time for set data: {:.3f}'.format(time.clock()-time0)); time0 = time.clock()
                    self.image_view.set_clim(vmin=self.vmin, vmax=self.vmax)
                    print('time for set clim: {:.3f}'.format(time.clock()-time0)); time0 = time.clock()
                    self.canvas.draw()
                    print('time for draw: {:.3f}'.format(time.clock()-time0)); time0 = time.clock()
class ImageViewPG(pyqtgraph.ImageView):
	def __init__(self, *args, **kwargs):
            super(ImageViewPG, self).__init__(*args, **kwargs)
            self.firstUpdate = True
            #self.getHistogramWidget().

        def show_image(self, data):
            if data is not None:
                if self.firstUpdate:
                    self.setImage(data)
                    self.firstUpdate = False
                else: self.setImage(data, autoRange=False, autoLevels=False, autoHistogramRange=False)


class ReconstructionApp(QMainWindow):
    def __init__(self, sino_path=''):
        print 'Sino_path = %s' %(sino_path)
        QMainWindow.__init__(self)
        self.setWindowTitle('Live View for Reconstruction')
        self.setMinimumSize(300, 300)
        self.resize(800, 1000)
        self.reconstructed = None
        self.sino_path = sino_path
        # layout and interface initialization
        vbox = QVBoxLayout()
        
        hbox_sinogram_path = QHBoxLayout()
        sinogram_label = QLabel('sinogram file:')
        self.sinogram_path = QLineEdit()
        self.select_sinogram_button = QPushButton('Select')
        self.refresh_button = QPushButton('Auto update')
        self.refresh_button.setCheckable(True)
        self.reset_button = QPushButton('Reset')
        #self.reset_button.setCheckable(False)
        hbox_sinogram_path.addWidget(sinogram_label)
        hbox_sinogram_path.addWidget(self.sinogram_path)
        hbox_sinogram_path.addWidget(self.select_sinogram_button)
        hbox_sinogram_path.addWidget(self.refresh_button)
        hbox_sinogram_path.addWidget(self.reset_button)
        #hbox_sinogram_path.addWidget(self.about_button)
        if SHOW_PATH_SELECTOR:
            vbox.addLayout(hbox_sinogram_path)

        hbox_slice = QHBoxLayout()
        slice_label = QLabel('slice:')
        self.slice_slider = QSlider(Qt.Horizontal)
        self.slice_slider.setTickPosition(QSlider.TicksBelow)
        self.slice_spinbox = QSpinBox()
        slice_label_2 = QLabel('number of slices:')
        self.max_slice_spinbox = QSpinBox()
        hbox_slice.addWidget(slice_label)
        hbox_slice.addWidget(self.slice_slider)
        hbox_slice.addWidget(self.slice_spinbox)
        hbox_slice.addWidget(slice_label_2)
        hbox_slice.addWidget(self.max_slice_spinbox)
        if SHOW_SLICE_SELECTOR:
            vbox.addLayout(hbox_slice)

        
        hbox_axis_shift_1 = QHBoxLayout()
        axis_shift_min_label = QLabel('axis shift range, from:')
        axis_shift_max_label = QLabel('to')
        self.axis_shift_min = QDoubleSpinBox()
        self.axis_shift_max = QDoubleSpinBox()
        self.axis_shift_min.valueChanged.connect(self.axis_shift_max.setMinimum)
        self.axis_shift_max.valueChanged.connect(self.axis_shift_min.setMaximum)
        axis_shift_step_label = QLabel(' step:')
        self.axis_shift_step = StepBox()
        hbox_axis_shift_1.addWidget(axis_shift_min_label)
        hbox_axis_shift_1.addWidget(self.axis_shift_min)
        hbox_axis_shift_1.addWidget(axis_shift_max_label)
        hbox_axis_shift_1.addWidget(self.axis_shift_max)
        hbox_axis_shift_1.addWidget(axis_shift_step_label)
        hbox_axis_shift_1.addWidget(self.axis_shift_step)
        if SHOW_AXIS_MANAGER_SELECTOR:
            vbox.addLayout(hbox_axis_shift_1)
        
        hbox_axis_shift_2 = QHBoxLayout()
        axis_shift_display_val_label = QLabel('axis shift [px]:')
        self.axis_shift_display_val = QDoubleSpinBox()
        self.axis_shift_slider = QSlider(Qt.Horizontal)
        self.axis_shift_slider.setTickPosition(QSlider.TicksBelow)
        self.axis_shift_valid_button = QPushButton('Lock offset')
        self.axis_shift_valid_button.setCheckable(True)
        hbox_axis_shift_2.addWidget(axis_shift_display_val_label)
        hbox_axis_shift_2.addWidget(self.axis_shift_display_val)
        hbox_axis_shift_2.addWidget(self.axis_shift_slider)
        hbox_axis_shift_2.addWidget(self.axis_shift_valid_button)
        if SHOW_AXIS_SELECTOR:
            vbox.addLayout(hbox_axis_shift_2)

        if use_pyqtgraph:
            print "use_pyqtgraph"
            self.view = ImageViewPG(self)
            self.resize(800, 800)
        else:
            self.view = ImageView(self)
            hbox_view_settings = QHBoxLayout()
            self.view_label = QLabel('display settings, percentile range:')
            to_label = QLabel('to')
            self.lower_display_range = LowerPercentileBox()
            self.upper_display_range = UpperPercentileBox()
            self.lower_display_range.valueChanged.connect(self.upper_display_range.setMinimum)
            self.upper_display_range.valueChanged.connect(self.lower_display_range.setMaximum)
            self.symmetric_display_range = QCheckBox('symmetric range')
            self.apply_view_settings_button = QPushButton('apply')
            hbox_view_settings.addWidget(self.view_label)
            hbox_view_settings.addWidget(self.lower_display_range)
            hbox_view_settings.addWidget(to_label)
            hbox_view_settings.addWidget(self.upper_display_range)
            hbox_view_settings.addWidget(self.symmetric_display_range)
            hbox_view_settings.addWidget(self.apply_view_settings_button)
            vbox.addLayout(hbox_view_settings)
            self.symmetric_display_range.stateChanged.connect(self.set_symmetric_display_range)
            self.apply_view_settings_button.clicked.connect(self.apply_display_settings)
            self.symmetric_display_range.setCheckState(Qt.Checked)
            self.lower_display_range.setValue(0.5)
            self.upper_display_range.setValue(99.5)
            self.lower_display_range.setMinimum(0.0)
            self.lower_display_range.setMaximum(100.0)
            self.upper_display_range.setMinimum(0.0)
            self.upper_display_range.setMaximum(100.0)
            self.apply_display_settings()
        vbox.addWidget(self.view)

        hbox_scan = QHBoxLayout()
        scan_label = QLabel('scan index:')
        self.scan_spinbox = QSpinBox()
        self.scan_slider = QSlider(Qt.Horizontal)
        self.scan_slider.setTickPosition(QSlider.TicksBelow)
        self.last_scan = 0
        self.first_scan = 0
        self.current_scan = 0
        self.asked_sino=0
        self.current_sino=0
        self.asked_scan = 0
        self.asked_recons = 0
        self.current_recons = 0
        self.last_offset=0

        self.locked_scan = True
        self.new_offset=False
        self.new_scan=False
        hbox_scan.addWidget(scan_label)
        hbox_scan.addWidget(self.scan_spinbox)
        hbox_scan.addWidget(self.scan_slider)
        if SHOW_SCAN_SELECTOR:
            vbox.addLayout(hbox_scan)

        #hbox_threshold = QHBoxLayout()
        #min_threshold_label = QLabel('Lower threshold:')
        #self.min_threshold_slider = QSlider(Qt.Horizontal)
        #self.min_threshold_slider.setTickPosition(QSlider.TicksBelow)
        #self.min_threshold_spinbox = QSpinBox()
        #max_threshold_label = QLabel('Upper threshold:')
        #self.max_threshold_slider = QSlider(Qt.Horizontal)
        #self.max_threshold_slider.setTickPosition(QSlider.TicksBelow)
        #self.max_threshold_spinbox = QSpinBox()
        #hbox_threshold.addWidget(min_threshold_label)
        #hbox_threshold.addWidget(self.min_threshold_slider)
        #hbox_threshold.addWidget(self.min_threshold_spinbox)
        #hbox_threshold.addWidget(max_threshold_label)
        #hbox_threshold.addWidget(self.max_threshold_slider)
        #hbox_threshold.addWidget(self.max_threshold_spinbox)
        #if SHOW_THRESHOLD_SELECTOR:
        #    vbox.addLayout(hbox_threshold)


        hbox_bottom = QHBoxLayout()
        bottom_label = QLabel('')
        bottom_label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.about_button = QPushButton('about')
        self.save_button = QPushButton('save image')
        close_button = QPushButton('close')
        #close_hbox.addWidget(filler)
        hbox_bottom.addWidget(bottom_label)
        hbox_bottom.addWidget(self.about_button)
        hbox_bottom.addWidget(self.save_button)
        hbox_bottom.addWidget(close_button)
        vbox.addLayout(hbox_bottom)

        central_widget = QWidget(self)
        central_widget.setLayout(vbox)
        self.setCentralWidget(central_widget)

        ##########################################################################################
        ##########################################################################################
        #
        #
        #
        #
        #
        # interface connections
        self.refresh_button.clicked.connect(self.set_autoUpdate)
        self.select_sinogram_button.clicked.connect(self.select_sinogram_file)
        self.max_slice_spinbox.valueChanged.connect(self.slice_slider.setMaximum)
        self.max_slice_spinbox.valueChanged.connect(self.slice_spinbox.setMaximum)
        self.slice_slider.valueChanged.connect(self.setSliceSpinBoxFromSlider)
        self.slice_spinbox.valueChanged.connect(self.setSliceSliderFromSpinBox)
        self.axis_shift_step.valueChanged.connect(self.set_axis_range)
        self.axis_shift_min.valueChanged.connect(self.set_axis_range)
        self.axis_shift_max.valueChanged.connect(self.set_axis_range)
        self.axis_shift_slider.valueChanged.connect(self.setAxisShiftSpinBoxFromSlider)
        self.axis_shift_display_val.valueChanged.connect(self.setAxisShiftSliderFromSpinBox)
        self.axis_shift_display_val.valueChanged.connect(self.axis_shift_changed)
        self.axis_shift_valid_button.clicked.connect(self.set_lock_axis_shift)
        self.scan_slider.valueChanged.connect(self.setScanSpinBoxFromSlider)
        self.scan_spinbox.valueChanged.connect(self.setScanSliderFromSpinBox)
        self.scan_spinbox.valueChanged.connect(self.scan_changed)
        self.slice_spinbox.valueChanged.connect(write_slice_info)
        #self.save_button.clicked.connect(self.save_reconstruction_image)
        close_button.clicked.connect(self.close)

        self.widgets_disabled_while_running = (self.sinogram_path, self.select_sinogram_button)

        self.widgets_disabled_while_axis_shift_adjust = (self.scan_spinbox, self.scan_slider, self.refresh_button)

        # sinogram refresh timer
        self.refresh_timer = QTimer(self)
        self.refresh_timer.timeout.connect(self.look_for_update)
        self.refresh_time = 10  # milliseconds
        self.last_sinogram_mod_time = -np.inf
        #self.refresh_timer.start(self.refresh_time)

        # threads
        self.reconstructor = Reconstructor(self)
        self.reconstructor.reconstructionFinished.connect(self.reconstruction_finished)
        self.reconstructor.startReconstruction.connect(self.setBusyCursor)
        self.reconstructor.reconstructionFinished.connect(self.resetCursor)
        self.reconstructor.initiationFinished.connect(self.initiate2)
        self.reco_is_dirty = True
        self.reconstructor.angle_range = (0., 180.)
        try:
            cuda
            self.reconstructor.start(QThread.LowPriority)
        except NameError: pass

        self.scan_checker = ScanChecker(self)
        self.scan_checker.newScanFound.connect(self.new_scan_found)

        self.sinogram_reader = SinogramReader(self)
        self.sinogram_reader.started.connect(self.setBusyCursor)
        self.sinogram_reader.finished.connect(self.resetCursor)
        #self.sinogram_reader.start(QThread.LowPriority)
        self.sino_is_dirty = False
        self.sinogram = None
        self.sinogram_reader.sinogramFinished.connect(self.read_sinogram_finished)


        # about window
        self.about_widget = QWidget()
        self.about_widget.setWindowTitle('about')
        about_vbox = QVBoxLayout()
        about_text = QTextEdit()
        about_text.setText(about_message)
        about_text.setReadOnly(True)
        close_hbox = QHBoxLayout()
        filler = QLabel('')
        filler.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        close_button = QPushButton('close')
        close_hbox.addWidget(close_button)
        about_vbox.addWidget(about_text)
        about_vbox.addLayout(close_hbox)
        self.about_widget.setLayout(about_vbox)
        self.about_widget.resize(450, 300)
        close_button.clicked.connect(self.about_widget.close)
        self.about_button.clicked.connect(self.about_widget.show)
        self.about_button.clicked.connect(self.about_widget.activateWindow)

        # final initialization
        self.sinogram_path.setText(sino_path)
        self.last_sinogram_path = ''
        self.last_save_path = ''
        self.max_slice_spinbox.setMaximum(99999)
        self.max_slice_spinbox.setValue(2048)
        self.slice_spinbox.setValue(1024)
        self.slice_slider.setSingleStep(50)
        self.slice_spinbox.setSingleStep(50)
        self.slice_slider.setTickInterval(50)
        self.axis_shift_step.setValue(0.5)
        self.axis_shift_min.setMinimum(-1e5)
        self.axis_shift_max.setMaximum(1e5)
        self.axis_shift_min.setValue(-50)
        self.axis_shift_max.setValue(50)
        self.axis_shift_display_val.setValue(0)
        self.axis_shift_step.setMinimum(0.1)
        self.axis_shift_step.setMaximum(9.99)
        
        #self.min_threshold_spinbox.setMaximum(65000)
        #self.min_threshold_spinbox.setValue(0.)
        #self.max_threshold_spinbox.setMaximum(65000)
        #self.max_threshold_spinbox.setValue(1.)
        #self.threshold_spinbox.setValue(0.5)
        #self.threshold_slider.setSingleStep(0.1)
        #self.threshold_spinbox.setSingleStep(0.1)
        #self.threshold_slider.setTickInterval(0.1)

        self.threshold_locked = False
        self.use_threshold = False

        self.scan_spinbox.setMinimum(self.first_scan)
        self.scan_spinbox.setMaximum(self.last_scan)
        self.scan_spinbox.setValue(self.first_scan)
        #self.scan_spinbox.setValue(1)
        self.scan_slider.setSingleStep(1)
        self.scan_slider.setMaximum(0)
        #self.scan_spinbox.setSingleStep(1)
        self.scan_slider.setTickInterval(1)

        self.refresh_button.setDisabled(True)
        self.scan_slider.setDisabled(True)
        self.scan_spinbox.setDisabled(True)
        
        self.save_button.setDisabled(True)

        try:
            cuda
        except NameError:
            QMessageBox(text='pycuda not found (ImportError), no reconstruction possible').exec_()

        
    def initiate(self):    
        print 'emit'
        self.reconstructor.sino_path = self.sino_path
        self.reconstructor.initReconstruction.emit()

    def initiate2(self):
        sino_path = self.sino_path
        print 'file_size = %d' %(self.reconstructor.file_size)
        file_size = self.reconstructor.file_size
        self.sinogram_reader.initiate(sino_path)
        self.scan_checker.initiate(sino_path,file_size)
        
        #try:
        #    cuda
            #cuda.init()
        #    self.reconstructor.start(QThread.LowPriority)
        #except NameError: pass
        self.sinogram_reader.start(QThread.LowPriority)

        self.sinogram_reader.read_last_sinogram()
        self.refresh_timer.start(self.refresh_time)

    def closeEvent(self, event):
        self.sinogram_reader.quit()
        self.reconstructor.quit()
        self.close()
        event.accept()

    # methods for processing
    def read_sinogram_finished(self):
        #self.last_scan = self.sinogram_reader.
        self.sinogram = self.sinogram_reader.sinogram
        self.current_sino = self.sinogram_reader.current_scan
        print 'Sino %d readden' %(self.current_sino)
        if self.asked_scan != self.current_sino:
            self.asked_sino = self.asked_scan
            print 'Sino %d asked for readding' %(self.asked_sino)
            self.sinogram_reader.read_sinogram(self.asked_sino)
        if self.asked_scan != self.current_recons and self.current_sino != self.current_recons and not self.reconstructor.isBusy:
            self.asked_recons = self.current_sino
            self.reconstructor.set_current_scan(self.asked_recons)
            self.reconstructor.set_new_sinogram(self.sinogram)
            print 'Sino %d asked for reconstruction' %(self.asked_recons)
            self.start_reconstruction()

    def reconstruction_finished(self):
        self.reconstructed = self.reconstructor.reconstructed
        self.view.show_image(self.reconstructed)
        self.scan_spinbox.setValue(self.reconstructor.current_scan)
        self.current_recons = self.reconstructor.current_scan
        if self.asked_recons != self.current_recons and self.current_sino == self.asked_recons:
            self.reconstructor.set_new_sinogram(self.sinogram)
            self.reconstructor.set_current_scan(self.current_sino)
            self.asked_recons=self.current_sino
            print 'Sino %d asked for reconstruction' %(self.asked_recons)
            self.start_reconstruction()

    def start_reconstruction(self):
        if not self.reconstructor.isBusy and self.reconstructor.hasSino:
            self.reconstructor.set_axis_shift(self.axis_shift_display_val.value())
            self.last_offset = self.axis_shift_display_val.value()
            print 'Offset is %f' %(self.last_offset)
            self.reconstructor.startReconstruction.emit()

    def new_scan_found(self):
        last_index = self.scan_checker.last_found_scan
        self.last_scan=last_index
        self.sinogram_reader.set_last_sinogram(last_index)
        self.set_scan_range()
        if self.autoUpdate:
            self.asked_scan = last_index

    def look_for_update(self):
        if self.asked_scan != self.current_sino and not self.sinogram_reader.isBusy:
            self.asked_sino = self.asked_scan
            print 'Sino %d asked for readding' %(self.asked_sino)
            self.sinogram_reader.read_sinogram(self.asked_sino)
        if ((self.asked_scan != self.current_recons and self.current_sino != self.current_recons) or (self.axis_shift_display_val.value() != self.last_offset)) and not self.reconstructor.isBusy:
            self.asked_recons = self.current_sino
            self.reconstructor.set_current_scan(self.asked_recons)
            self.reconstructor.set_new_sinogram(self.sinogram)
            self.last_offset = self.axis_shift_display_val.value()
            print 'Sino %d asked for reconstruction' %(self.asked_recons)
            self.start_reconstruction()

    def look_for_updateb(self):
        #test if some update
        sinogram_filepath = self.sinogram_path.text()
        if sinogram_filepath == '':
            QMessageBox(text='no sinogram file given', parent=self).exec_()
            if self.refresh_button.isChecked():
                self.refresh_button.click()
            return
        elif not os.path.isfile(sinogram_filepath):
            QMessageBox(text='sinogram file does not exist: '+sinogram_filepath, parent=self).exec_()
            if self.refresh_button.isChecked():
                self.refresh_button.click()
            return
        mod_time = os.path.getmtime(sinogram_filepath)
        if mod_time > self.last_sinogram_mod_time:
            if self.sinogram_reader.isRunning():
                self.sino_is_dirty = True
            else:
                self.sinogram_reader.read_sinogram(sinogram_filepath)
            self.last_sinogram_mod_time = mod_time


    #def look_for_changed_sinogram(self):
    #    sinogram_filepath = self.sinogram_path.text()
    #    if sinogram_filepath == '':
    #        QMessageBox(text='no sinogram file given', parent=self).exec_()
    #        if self.refresh_button.isChecked():
    #            self.refresh_button.click()
    #        return
    #    elif not os.path.isfile(sinogram_filepath):
    #        QMessageBox(text='sinogram file does not exist: '+sinogram_filepath, parent=self).exec_()
    #        if self.refresh_button.isChecked():
    #            self.refresh_button.click()
    #        return
    #    mod_time = os.path.getmtime(sinogram_filepath)
    #    if mod_time > self.last_sinogram_mod_time:
    #        if self.sinogram_reader.isRunning():
    #            self.sino_is_dirty = True
    #        else:
    #            self.sinogram_reader.read_sinogram(sinogram_filepath)
    #        self.last_sinogram_mod_time = mod_time


    def axis_shift_changed(self):
        axis_shift = self.axis_shift_display_val.value()
        if self.sinogram is not None:
            self.reconstructor.set_axis_shift(axis_shift)
            self.start_reconstruction()

    def scan_changed(self):
        self.asked_scan = self.scan_slider.value() 

    def save_reconstruction_image(self):
        filename = QFileDialog.getSaveFileName(self, 'save image to file ...',
                        self.last_save_path)
        if filename is not '':
            self.last_save_path = filename
            save_reconstruction_image(filename)
            # put code to save file here

    def select_sinogram_file(self):
        filename = QFileDialog.getOpenFileName(self, 'select sinogram file', self.last_sinogram_path)
                                                # ^^ you can add:  , filter='.edf (*.edf)'
        if filename is not '':  # '' = return value when clicking on cancel button
            self.sinogram_path.setText(filename)
            self.last_sinogram_path = filename

    def set_autoUpdate(self):
        if self.refresh_button.isChecked():
            if not self.scan_checker.isRunning:
                self.scan_checker.look_for_new_scan()
                self.autoUpdate=True
                self.axis_shift_valid_button.setDisabled(True)
        else:
            self.autoUpdate=False
            self.axis_shift_valid_button.setDisabled(False)

    def set_refreshing(self):
        if self.refresh_button.isChecked():
            self.refresh_button.setText('stop refreshing')
            self.refresh_timer.start(self.refresh_time)
            for widget in self.widgets_disabled_while_running:
                widget.setDisabled(True)
        else:
            self.refresh_button.setText('start refreshing')
            self.refresh_timer.stop()
            for widget in self.widgets_disabled_while_running:
                widget.setEnabled(True)

    def set_lock_axis_shift(self):
        if self.axis_shift_valid_button.isChecked():
            self.axis_shift_valid_button.setText('Adjust offset')
            self.axis_shift_locked=True
            self.axis_shift_display_val.setDisabled(True)
            self.axis_shift_slider.setDisabled(True)
            self.scan_slider.setDisabled(False)
            self.scan_spinbox.setDisabled(False)
            self.refresh_button.setDisabled(False)
        else:
            self.axis_shift_valid_button.setText('Lock offset')
            self.axis_shift_locked=False
            self.axis_shift_display_val.setDisabled(False)
            self.axis_shift_slider.setDisabled(False)
            self.scan_slider.setDisabled(True)
            self.scan_spinbox.setDisabled(True)
            self.refresh_button.setDisabled(True)
        self.set_locked()

    def set_locked(self):
        self.locked_scan = self.axis_shift_locked and ((not self.use_threshold) or self.threshold_locked)

    def update_lower(self, val):
        if not np.isclose(100 - val, self.lower_display_range.value()):
            self.lower_display_range.setValue(100 - val)

    def update_upper(self, val):
        if not np.isclose(100 - val, self.upper_display_range.value()):
            self.upper_display_range.setValue(100 - val)

    def set_symmetric_display_range(self):
        if self.symmetric_display_range.isChecked():
            self.lower_display_range.valueChanged.connect(self.update_upper)
            self.upper_display_range.valueChanged.connect(self.update_lower)
        else:
            self.lower_display_range.valueChanged.disconnect(self.update_upper)
            self.upper_display_range.valueChanged.disconnect(self.update_lower)

    def set_axis_range(self):
        step_value = self.axis_shift_step.value()
        if step_value > 0:
            vmin, vmax = self.axis_shift_min.value(), self.axis_shift_max.value()
            range = vmax - vmin
            step_number = int(range/step_value)
            if step_number > 1000:
                step_value = range/1000
                step_number = 1000
                self.axis_shift_step.setValue(step_value)
            correct_max_shift = step_number*step_value + vmin
            axis_shift_val = self.axis_shift_display_val.value()
            if range > 1:
                self.axis_shift_step.setMaximum(range/4)
            self.axis_shift_display_val.setSingleStep(step_value)
            self.axis_shift_min.setSingleStep(step_value*3)
            self.axis_shift_max.setSingleStep(step_value*3)
            #self.axis_shift_max.setValue(correct_max_shift)
            self.axis_shift_slider.setMaximum(step_number)
            self.axis_shift_slider.setValue(int(np.rint((axis_shift_val - vmin)/step_value)))
            self.axis_shift_display_val.setMinimum(vmin)
            self.axis_shift_display_val.setMaximum(vmax)

    def set_scan_range(self):
        step_value = 1
        if step_value > 0:
            vmin, vmax = self.first_scan, self.last_scan
            range = vmax - vmin
            step_number = int(range/step_value)
            if step_number > 1000:
                step_value = range/1000
                step_number = 1000
                self.scan_step.setValue(step_value)
            correct_max_scan = step_number*step_value + vmin
            scan_val = self.current_scan
            #self.axis_shift_display_val.setSingleStep(step_value)
            #self.axis_shift_min.setSingleStep(step_value*3)
            #self.axis_shift_max.setSingleStep(step_value*3)
            #self.axis_shift_max.setValue(correct_max_shift)
            self.scan_slider.setMaximum(step_number)
            self.scan_slider.setValue(int(np.rint((scan_val - vmin)/step_value)))
            self.scan_spinbox.setMinimum(vmin)
            self.scan_spinbox.setMaximum(vmax)


    def apply_display_settings(self):
        vmin, vmax = self.lower_display_range.value(), self.upper_display_range.value()
        if not np.isclose(vmin, vmax):
            self.view.autoscale_percentiles = (vmin, vmax)
            self.view.autoscale = True
            self.view.external_display_range = False
            self.view.show_image(self.reconstructed)
        else:
            QMessageBox(text='lower and upper limit cannot be identical', parent=self).exec_()

    def setAxisShiftSliderFromSpinBox(self, spinbox_val):
        step_value, vmin = self.axis_shift_step.value(), self.axis_shift_min.value()
        if step_value > 0:
            new_value = int((spinbox_val - vmin)/step_value)
            if new_value != self.axis_shift_slider.value():
                 self.axis_shift_slider.setValue(new_value)

    def setAxisShiftSpinBoxFromSlider(self, slider_index):
        step_value, vmin = self.axis_shift_step.value(), self.axis_shift_min.value()
        if step_value > 0:
            new_value = vmin + step_value*slider_index
            if not np.isclose(new_value, self.axis_shift_display_val.value()):
                self.axis_shift_display_val.setValue(new_value)

    def setSliceSliderFromSpinBox(self, spinbox_index):
        if spinbox_index != self.slice_slider.value():
             self.slice_slider.setValue(spinbox_index)

    def setSliceSpinBoxFromSlider(self, slider_index):
        if slider_index != self.slice_spinbox.value():
             self.slice_spinbox.setValue(slider_index)

    def setScanSliderFromSpinBox(self, spinbox_index):
        if spinbox_index != self.scan_slider.value():
             self.scan_slider.setValue(spinbox_index)
             self.current_scan=spinbox_index

    def setScanSpinBoxFromSlider(self, slider_index):
        if slider_index != self.scan_spinbox.value():
             self.scan_spinbox.setValue(slider_index)
             self.current_scan = slider_index

    def resetCursor(self):
        self.setCursor(Qt.ArrowCursor)

    def setBusyCursor(self):
        self.setCursor(Qt.BusyCursor)

    def show_about_message(self):
        QMessageBox(text=about_message, parent=self).show()


class StepBox(QDoubleSpinBox):
    def stepBy(self, steps):
        self.setValue(self.value()*BOX_SCROLL_SPEED**steps)


class LowerPercentileBox(QDoubleSpinBox):
    def stepBy(self, steps):
        if self.value() > 0.:
            self.setValue(self.value()*BOX_SCROLL_SPEED**steps)
        else:
            self.setValue(self.value()+0.1*steps)


class UpperPercentileBox(QDoubleSpinBox):
    def stepBy(self, steps):
        if self.value() > 0.:
            self.setValue(100 - (100 - self.value())*BOX_SCROLL_SPEED**-steps)
        else:
            self.setValue(self.value()+0.1*steps)



if __name__ == '__main__':
    def main():
        import sys
        import atexit
        n = len(sys.argv)
        if n==2 :
            app = QApplication(sys.argv)
            app.setApplicationName("Live Reconstruction App")
            ui = ReconstructionApp(sys.argv[1])
            print 'Creation OK'
            ui.initiate()
            print 'Initiation OK'
            ui.show()
            sys.exit(app.exec_())
        else:
            print 'Usage : liveslice fullpath_to_first_sinogram.edf'
    #print("Live slice")
    
    main()

    #print("Finished")
     
    sys.exit()



