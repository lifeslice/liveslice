#!/usr/bin/env python

#from distutils.core import setup
from setuptools import setup


setup(
    name='live_slice',
    version='1.0',
    author='Pierre LHUISSIER (SIMaP/GPM2 - CNRS - Grenoble University) ; Jonas Dittmann (LRM - Univ. Wuerzburg) ; Maximilian Ullherr (LRM - Univ. Wuerburg) ',
    download_url='',
    packages=['live_slice']
    )
